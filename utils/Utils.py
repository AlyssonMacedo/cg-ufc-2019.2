import numpy as np
from light.Lighting import *


def camera_init_(view, l_at, v_up):
    k = view - l_at
    kc = k / np.linalg.norm(k)

    v = v_up - view
    i = np.cross(v, kc)
    ic = i / np.linalg.norm(i)

    jc = np.cross(kc, ic)

    """
    # cam->world
    ic = np.append(ic, [0])
    jc = np.append(jc, [0])
    kc = np.append(kc, [0])
    vi = np.append(view, [1])
    """

    # world->cam
    ic = np.append(ic, [-np.dot(view, ic)])
    jc = np.append(jc, [-np.dot(view, jc)])
    kc = np.append(kc, [-np.dot(view, kc)])
    vi = np.array([0, 0, 0, 1])

    cam = ic
    cam = np.vstack((cam, jc))
    cam = np.vstack((cam, kc))
    cam = np.vstack((cam, vi))
    # cam = np.transpose(cam)

    return cam


def calcula_cor(obj, f_lum, pint, normal, p0):
    cor = [0, 0, 0]
    for f in f_lum:
        if isinstance(f, EnvironmentLight):
            il = f.iluminar(obj.material.mat_amb)
            for x in range(3):
                cor[x] = il[x] + cor[x]
        else:
            if isinstance(f, PointLight):
                ild = f.iluminar_dif(obj.material.mat_dif, normal, pint)
                ils = f.iluminar_sp(obj.material.mat_sp, normal, p0,pint, 0.5)
                for x in range(3):
                    cor[x] = ild[x] + ils[x] + cor[x]

    return cor
